### Hi there 👋

[![StandWithUkraine](https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/badges/StandWithUkraine.svg)](https://github.com/vshymanskyy/StandWithUkraine/blob/main/docs/README.md)

- 🔭 I’m currently working on NTT DATA Corporation, and also working voluntery, non-profit activities as representative director of OpenStreetMap Foundation Japan.
- I'm enthuasist for Free Software and these are my private and hobby projects which run in my spare time.
- These projects are unrelated with NTT DATA and its interectual property.

![Github Stats Card](https://github-readme-stats.vercel.app/api?username=miurahr&show_icons=true)


### Popular projects

[![aqtinstall](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=aqtinstall)](https://github.com/miurahr/aqtinstall)
[![Pykakasi](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=pykakasi)](https://github.com/miurahr/pykakasi)
[![Py7zr](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=py7zr)](https://github.com/miurahr/py7zr)


### OpenStreetMap/GIS


[![Simple Task Manager](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=simple-task-manager)](https://github.com/miurahr/simple-task-manager)
[![PanoramaViewer](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=panoramaviewer)](https://github.com/miurahr/panoramaviewer)


### Devops automation

[![aqtinstall](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=aqtinstall)](https://github.com/miurahr/aqtinstall)
[![install-linuxdeploy-action](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=install-linuxdeploy-action)](https://github.com/miurahr/install-linuxdeploy-action)


### Computer Aided Translation


[![OmegaT](https://github-readme-stats.vercel.app/api/pin/?username=omegat-org&repo=omegat)](https://github.com/omegat-org/omegat)
[![OmegaT TexTra Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-textra-plugin)](https://github.com/miurahr/omegat-textra-plugin)
[![OmegaT EPWING Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-plugin-epwing)](https://github.com/miurahr/omegat-plugin-epwing)
[![Omegat OnlineDictionary Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-onlinedictionary)](https://github.com/miurahr/omegat-onlinedictionary)
[![Omegat Sambadoc Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-sambadoc-plugin)](https://github.com/miurahr/omegat-sambadoc-plugin)
[![Omegat PDIC dictionary Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-pdic)](https://github.com/miurahr/omegat-pdic)
[![Omegat MDict dictionary Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-mdict)](https://github.com/miurahr/omegat-mdict)
[![OmegaT Adaptive theme plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-adaptive-theme)](https://github.com/miurahr/omegat-adaptive-theme)
[![OmegaT round theme plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-round-theme)](https://github.com/miurahr/omegat-round-theme)

[![OmegaT primer book](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-for-cat-beginners)](https://github.com/miurahr/omegat-for-cat-beginners)
[![Omegat Plugin project skeleton](https://github-readme-stats.vercel.app/api/pin/?username=omegat-org&repo=plugin-skeleton)](https://github.com/omegat-org/plugin-skeleton)

[![Gradle-Omegat Plugin](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=gradle-omegat-plugin)](https://github.com/miurahr/gradle-omegat-plugin)
[![omegat-stat](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=omegat-stat)](https://github.com/miurahr/omegat-stat)


### Dictionary/Ebook access in Java

[![DictZip](https://github-readme-stats.vercel.app/api/pin/?username=dictzip&repo=dictzip-java)](https://github.com/dictzip/dictzip-java)
[![EB4j](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=eb4j)](https://github.com/eb4j/eb4j)
[![Furoku-data](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=furoku-data)](https://github.com/eb4j/furoku-data)
[![MDict4j](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=mdict4j)](https://github.com/eb4j/mdict4j)
[![DSL4j](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=dsl4j)](https://github.com/eb4j/dsl4j)
[![EBviewer](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=ebviewer)](https://github.com/eb4j/ebviewer)
[![EB4j-tools](https://github-readme-stats.vercel.app/api/pin/?username=eb4j&repo=eb4j-tools)](https://github.com/eb4j/eb4j-tools)

### Natural Language Processing

[![Pykakasi](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=pykakasi)](https://github.com/miurahr/pykakasi)
[![Unihandecode](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=unihandecode)](https://github.com/miurahr/unihandecode)

### Compression library and related

[![Py7zr](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=py7zr)](https://github.com/miurahr/py7zr)
[![multi volume file](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=multivolume)](https://github.com/miurahr/multivolume)
[![pyppmd](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=pyppmd)](https://github.com/miurahr/pyppmd)
[![pybcj](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=pybcj)](https://github.com/miurahr/pybcj)
[![inflate64](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=inflate64)](https://github.com/miurahr/inflate64)


### Raspberry Pi

[![PiCast](https://github-readme-stats.vercel.app/api/pin/?username=miurahr&repo=picast)](https://github.com/miurahr/picast)


